
Node breadcrumb 6.x-1.0-dev
---------------------------

Description
-----------
Allow to customize breadcrumb and menu location of nodes depending on
their content type, associated terms and others conditions.


Installation
------------
1. Extract & copy module's folder to <drupal>/sites/all/modules/.
2. Activate module.
3. Grant access rights for module's administration (optionally).
4. Configure module at admin/settings/node_breadcrumb page.

Contact
-------
Barhatov Andrew, barhatov@krasu.ru
(Siberian Federal University, Russia)
